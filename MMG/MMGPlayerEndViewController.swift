//
//  MMGPlayerEndViewController.swift
//  DameDashStudios
//
//  Created by Sanchan on 03/03/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class MMGPlayerEndViewController: UIViewController {
    
    var nextCollectionList = NSMutableArray()
    var nextrowID = Int()
    var userID = String()
    var nextData = NSDictionary()
    var videoDict = NSDictionary()

    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var timeLbl: UILabel!
    var myTimer = Timer()
    var i:Int = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//            if nextrowID == (nextCollectionList.count - 1)
//            {
//                self.videoId = ((self.nextCollectionList.firstObject as! NSDictionary)["id"] as! String)
//                nextrowID = 0
//            }
//            else
//            {
//               self.videoId = ((self.nextCollectionList[self.nextrowID + 1] as! NSDictionary)["id"] as! String)
//                nextrowID = +1
//                print(nextrowID)
//            }
//       
        
        
        // Do any additional setup after loading the view.
    }

    func getData(getnextData:NSDictionary)
    {
        self.nextData = getnextData
        RecentlyWatched(withurl:kRecentlyWatchedUrl)
        self.myTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.getplay), userInfo: nil, repeats: true)
    }
    
  /*  func getassetdata(id:String,userid:String)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        print(parameters)
        MMGApiManager.sharedManager.postDataWithJson(url: "http://dev.damedashstudios.com/getAssestData", parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let jsonresponse = JSON as! NSArray
                for dict in jsonresponse
                {
                    self.videoDict = dict as! NSDictionary
                    self.bgImg.kf.indicatorType = .activity
                    self.bgImg.kf.setImage(with: URL(string: self.videoDict["main_carousel_image_url"] as! String))
                    print(self.videoDict)
                    
                }
            }
            else
            {
                print("json Error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }*/
    func getplay()
    {
        timeLbl.text = "\(i) SEC"
        i = i-1
        if i == 0
        {
          myTimer.invalidate()
          print("player is ready")
          playvideo()
        }
        
    }
    
    @IBAction func playBtn(_ sender: Any) {
        playvideo()
    }
    
    func playvideo()
    {
        let PlayerVC = MMGPlayerViewController()
        if (videoDict["subscription_status"] as! String) == "active"
        {
            self.RecentlyWatched(withurl: kRecentlyWatchedUrl)
            PlayerVC.isResume = false
            if nextData["url_m3u8"] != nil && nextData["url_m3u8"] as! String != ""
            {
                PlayerVC.videoUrl = nextData["url_m3u8"] as! String
            }
            else
            {
                PlayerVC.videoUrl = nextData["url"] as! String
            }
            PlayerVC.mainVideoID = nextData["id"] as! String
            PlayerVC.playVideo(userId:userID,videoId: nextData["id"] as! String,mycollectionlist:nextCollectionList,rowid:nextrowID)
            //   self.navigationController?.pushViewController(PlayerVC, animated: true)
            self.navigationController?.pushViewController(PlayerVC, animated: false)
            //  self.present(PlayerVC, animated: true, completion: nil)
        }
    }
    func RecentlyWatched(withurl:String)
    {
        let parameters = ["createRecentlyWatched": ["videoId":nextData["id"] as! String,"userId":userID as AnyObject]]
        MMGApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters as [String : [String : AnyObject]]){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                let dict = JSON as! NSDictionary
                let dicton = dict["recentlyWatched"] as! NSDictionary
                if (dicton["seekTime"] as! Float64) > 0
                {
                  //  self.isRecentlywatch = true
                }
              //  self.delegate?.recentlyWatcheddata(recentlyWatchedDict: dicton)
            }
            else
            {
                print("json error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
