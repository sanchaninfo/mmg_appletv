//
//  MMGCollectionContentCell.swift
//  Sonic Pool
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
import SystemConfiguration
import Kingfisher


class MMGCollectionContentCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    var menuCollectionList = NSMutableArray()
    var UserId,DeviceId,uuid,imageUrl : String!
    var CarousalDict = [[String:Any]]()
    var arrayImages = [NSData]()
    var images = [UIImage]()
    var i:Int = 0
    var mytimer = Timer()
    var newtimer = Timer()
    var nextassetData = NSDictionary()
    var isMyList = Bool()
    var count: Int = 0
    var isCalled = Bool()
    var ImagesArray = NSMutableArray()
    var bgSlideShow = NSMutableArray()
    var myQueue = DispatchQueue.self
    var indexValue = Int()
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuCollectionList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentCell", for: indexPath)
        let Path = menuCollectionList[indexPath.row] as! NSDictionary
        if Path[kMetadata] != nil
        {
            let metaDataPath = Path[kMetadata] as! NSDictionary
            if metaDataPath[kIsmenu] != nil
            {
                if metaDataPath[kIsmenu] as! Bool == true
                {
                    (cell.viewWithTag(11) as! UIImageView).image = metaDataPath[kMovieart] as? UIImage
                }
            }
            else
            {
                let img = (cell.viewWithTag(11) as! UIImageView)
                img.kf.indicatorType = .activity
                (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: metaDataPath[kMovieart] as! String))
            }
        }
        else
        {
          
            let img = (cell.viewWithTag(11) as! UIImageView)
            img.kf.indicatorType = .activity
            (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: Path["posterURL"] as! String))
        }
        collectionView.isScrollEnabled = true
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        let tableview:UITableView = self.superview?.superview as! UITableView
        //Previous Index Focus
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath)
        {
            cell.transform = .identity
        }
        
        //Next Index Focus
        if let indexPath = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: indexPath)
        {
            collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
            (cell.viewWithTag(11) as! UIImageView).adjustsImageWhenAncestorFocused = true
            let viewcontroller = tableview.dataSource as! MMGMainViewController
            let Path = menuCollectionList[indexPath.row] as! NSDictionary
            if Path[kMetadata] != nil
            {
                let image = Path[kMetadata] as! NSDictionary
                if image[kCarouselId] as! String == "Menu"
                {
                    if indexPath.row == 0
                    {
                        viewcontroller.MainImage.kf.indicatorType = .activity
                        viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "categories_shelf")
                        viewcontroller.staticLbl.text = "Categories"
                        viewcontroller.lbl2.text = ""
                    }
                    if indexPath.row == 1
                    {
                        viewcontroller.MainImage.kf.indicatorType = .activity
                        viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "settings_Shelf")
                        viewcontroller.staticLbl.text = "Settings"
                        viewcontroller.lbl2.text = "Your account information"
                    }
                    if indexPath.row == 2
                    {
                        viewcontroller.MainImage.kf.indicatorType = .activity
                        viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "Search_shelf")
                        viewcontroller.staticLbl.text = "Search"
                        viewcontroller.lbl2.text = "Search for TV shows,movies"
                    }
                    viewcontroller.AssestName.text = ""
                    viewcontroller.ReleaseDate.text = ""
                    viewcontroller.TimeLbl.text = ""
                    viewcontroller.Description.text = ""
                    (viewcontroller.view.viewWithTag(1))?.isHidden = true
                }
                else
                {
                    viewcontroller.staticLbl.text = ""
                    viewcontroller.lbl2.text = ""
                    imageUrl = image["main_carousel_image_url"] as! String
                       viewcontroller.MainImage.kf.indicatorType = .activity
                       viewcontroller.MainImage.kf.setImage(with: URL(string: image["main_carousel_image_url"] as! String))
                    ImagesArray = ["https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/3b2acb51-069d-4022-9b2d-7a9178d16fa7.jpg",
                                   "https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/e6d082c7-bb52-426f-96a0-363fdd6e848b.png",
                                   "https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/fffd99bb-b3f3-4885-be58-64fbf91c07c7.png"]
                    
                    DispatchQueue.main.async {
                        self.bgSlideShow = ["https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/3b2acb51-069d-4022-9b2d-7a9178d16fa7.jpg",
                                            "https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/e6d082c7-bb52-426f-96a0-363fdd6e848b.png",
                                            "https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/fffd99bb-b3f3-4885-be58-64fbf91c07c7.png"]
                        if indexPath.row != 0
                        {
                            self.indexValue = 1
                        }
                      //  self.animateImages()
                    }
                    
                    //                DispatchQueue.global(qos: .userInitiated).async {
                    //
                    //                for i in 0..<bgSlideShow.count
                    //                {
                    //                    do
                    //                        {
                    //                            var imageData = NSData()
                    //                            imageData =  try NSData(contentsOf: URL(string: (bgSlideShow[i] as! String))!)
                    //                           // self.arrayImages.append(imageData)
                    //                            self.images.append(UIImage(data: imageData as Data)!)
                    //                        }
                    //                        catch
                    //                        {
                    //                            print(error)
                    //                        }
                    //                }
                    //
                    //            }
                    //                mytimer.invalidate()
                    
                    
                    
                    // self.mytimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.swapImage), userInfo: nil, repeats: true)
                    
                    //   mytimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.swapImage), userInfo: nil, repeats: true)
                    // mytimer.fire()
                    
                    viewcontroller.AssestName.text = (Path["name"] as? String)?.capitalized
                    let str1 = (image["release_date"] as! String).components(separatedBy: "-")
                    viewcontroller.ReleaseDate.text = str1[0]
                    viewcontroller.TimeLbl.text =  stringFromTimeInterval(interval: Double((Path["file_duration"] as! String))!) as String
                    let DescriptionText = Path["description"] as? String
                    let destxt = DescriptionText?.replacingOccurrences(of: "&", with: "", options: .literal, range: nil)
                    let destxt1 =  destxt?.replacingOccurrences(of: "amp", with: "", options: .literal, range: nil)
                    let destxt2 = destxt1?.replacingOccurrences(of: ";", with: "", options: .literal, range: nil)
                    
                    viewcontroller.Description.text = destxt2
                    if (DescriptionText?.contains("n/a"))!
                    {
                        viewcontroller.Description.text = ""
                    }
                    let labelText = (Path["name"] as? String)?.capitalized
                    let desLbl = viewcontroller.Description
                    let lbl = viewcontroller.AssestName
                    lbl?.frame = CGRect(x: (lbl?.frame.origin.x)!, y: (lbl?.frame.origin.y)!, width: (labelText?.widthWithConstrainedWidth(height: 60, font: (lbl?.font)!))!, height: (lbl?.frame.size.height)!)
                    desLbl?.frame = CGRect(x: (desLbl?.frame.origin.x)!, y: (desLbl?.frame.origin.y)!, width: (desLbl?.frame.size.width)!, height: (DescriptionText?.widthWithConstrainedWidth(height: 60, font: (desLbl?.font)!))!)
                    (viewcontroller.view.viewWithTag(1))?.isHidden = false
                }
            }
            else
            {
                viewcontroller.staticLbl.text = ""
                viewcontroller.lbl2.text = ""
                viewcontroller.MainImage.kf.indicatorType = .activity
                viewcontroller.MainImage.kf.setImage(with: URL(string: "http://s3.amazonaws.com/peafowl/damedash-devstore/cover.jpg"))
                viewcontroller.AssestName.text = "DIPSET"
                viewcontroller.ReleaseDate.text = "20/11/17"
                viewcontroller.TimeLbl.text = "50 min"
                let DescriptionText = "Dipset Description"
                (viewcontroller.view.viewWithTag(1))?.isHidden = false
            }
          
        }
    }
    func swapImage() {
     
        
        DispatchQueue.main.async {
            let tableview:UITableView = self.superview?.superview as! UITableView
            let viewcontroller = tableview.dataSource as! MMGMainViewController
          /*  UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {() -> Void in
           //   viewcontroller.MainImage.alpha = !viewcontroller.MainImage.alpha
            }, completion: {(_ done: Bool) -> Void in
                //
            })*/
            viewcontroller.MainImage.animationImages = self.images
            viewcontroller.MainImage.animationDuration = 3.0
            viewcontroller.MainImage.animationRepeatCount = 0
            viewcontroller.MainImage.startAnimating()
         //   viewcontroller.MainImage.image = self.images[self.i]
//            let transition = CATransition()
//            transition.duration = 1.0
//            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//            transition.type = kCATransitionFade
//            viewcontroller.MainImage.layer.add(transition, forKey: nil)
        }
        
      /*  i += 1
        if i == arrayImages.count
        {
            i = 0
        }*/
    }
    
    func animateImages()
    {
        if isCalled
        {
            self.newtimer.invalidate()
        }
        //animateImagesUrl()
    //    mytimer = Timer.init(timeInterval: 3.0, repeats: false, block: {_ in self.animateImagesUrl()})
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {self.animateImagesUrl()})
      //  mytimer = Timer(timeInterval: 3.0, target: self, selector: #selector(self.animateImagesUrl), userInfo: nil, repeats: true)
    //    animateImagesUrl()
    }
    func animateImagesUrl() -> Void
    {
        mytimer.invalidate()
        let i = 0
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MMGMainViewController
        let bgSlideShow:NSMutableArray = ["https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/3b2acb51-069d-4022-9b2d-7a9178d16fa7.jpg",
                                          "https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/e6d082c7-bb52-426f-96a0-363fdd6e848b.png",
                                          "https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/fffd99bb-b3f3-4885-be58-64fbf91c07c7.png"]
        if count > 0
        {
            mytimer.invalidate()
            if self.count == 3
            {
                self.count = 0
            }
        }
        UIView.transition(with: viewcontroller.MainImage, duration: 2.0, options: .transitionCrossDissolve, animations:{
        
         //   viewcontroller.MainImage.animationImages = self.images
          // viewcontroller.MainImage.startAnimating()
            viewcontroller.MainImage.kf.setImage(with: URL(string: bgSlideShow[self.count] as! String))}, completion:
            {(finished:Bool) -> Void in
                self.count += 1
                if self.count == bgSlideShow.count || self.isCalled
                {
                    self.count = 0
                    return
                }
                else
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {self.animateImagesUrl()})
                }
              //  self.isCalled = true
            
      /*
        if (self.indexValue == 1)
        {
         //   self.indexValue = 0
            return;
        }
        else
        {
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {self.animateImagesUrl()})
          self.isCalled = false
        }*/
                //    let mytimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.animateImagesUrl), userInfo: nil, repeats: true)
     //   self.myfunction()
        })
        
        
    }
    func myfunction()
    {
        if self.indexValue == 0
        {
            self.animateImagesUrl()
        }
    }
    
     func timerfunction()
     {
        self.newtimer = Timer.init(timeInterval: 1.1, target: self, selector: #selector(self.animateImagesUrl), userInfo: nil, repeats: true)
        self.isCalled = true
        print(self.isCalled)
     }
    
    func crossfade() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {() -> Void in
            let tableview:UITableView = self.superview?.superview as! UITableView
            let viewcontroller = tableview.dataSource as! MMGMainViewController
            viewcontroller.MainImage.alpha = viewcontroller.MainImage.alpha
        }, completion: {(_ done: Bool) -> Void in
            //
        })
    }
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Path = menuCollectionList[indexPath.row] as! NSDictionary
        if Path[kMetadata] != nil
        {
            let MetaDict = Path[kMetadata] as! NSDictionary
            if ((MetaDict[kCarouselId] as! String) == "Menu")
            {
                if indexPath.row == 0
                {
                    gotoCategories()
                }
                if indexPath.row == 1
                {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let SettingsPage = storyBoard.instantiateViewController(withIdentifier: "Settings") as! MMGSettingsViewController
                    let tableview:UITableView = self.superview?.superview as! UITableView
                    let viewcontroller = tableview.dataSource as! MMGMainViewController
                    SettingsPage.deviceId = viewcontroller.deviceId
                    SettingsPage.uuid = viewcontroller.uuid
                    viewcontroller.navigationController?.pushViewController(SettingsPage, animated: true)
                }
                if indexPath.row == 2
                {
                    let tableview:UITableView = self.superview?.superview as! UITableView
                    let viewcontroller = tableview.dataSource as! MMGMainViewController
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    guard let searchResultsController = storyboard.instantiateViewController(withIdentifier: "Search") as? MMGSearchListViewController
                        else {
                            fatalError("Unable to instatiate a SearchResultsViewController from the storyboard.")
                    }
                    let searchController = UISearchController(searchResultsController: searchResultsController)
                    searchController.searchResultsUpdater = searchResultsController
                    searchController.view.backgroundColor = UIColor.init(red: 13/255, green: 13/255, blue: 13/255, alpha: 1)
                    searchController.searchBar.setScopeBarButtonTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white], for: .normal)
                    searchController.searchBar.keyboardAppearance = UIKeyboardAppearance.dark
                    searchController.searchBar.placeholder = NSLocalizedString("Enter keyword (e.g. The Secret to Ballin)", comment: "")
                    let searchContainer = UISearchContainerViewController(searchController: searchController)
                    searchContainer.title = NSLocalizedString("Search", comment: "")
                    searchResultsController.searchCollectionList = viewcontroller.searchCollectionList
                    searchResultsController.userId = viewcontroller.userId
                    searchResultsController.deviceId = viewcontroller.deviceId
                    searchResultsController.uuid = viewcontroller.uuid
                    searchResultsController.searchDelegate = viewcontroller
                    viewcontroller.navigationController?.pushViewController(searchContainer, animated: false)
                }
            }
            else
            {
                //            if indexPath.row == (menuCollectionList.count - 1)
                //            {
                //                nextassetData = menuCollectionList.firstObject as! NSDictionary
                //            }
                //            else
                //            {
                //                nextassetData = menuCollectionList[indexPath.row + 1] as! NSDictionary
                //            }
                //  getaccountInfo(id:Path["id"] as! String,userid: UserId,nextData:nextassetData["id"] as! String)
                getaccountInfo(id: Path["id"] as! String, userid: UserId,list:menuCollectionList,rowid:indexPath.row)
                //     getaccountInfo(id:Path["id"] as! String,userid: UserId/*,tvshow: (Path["tv_show"] as! Bool)*/)
                // getAssetData(withUrl:kAssestDataUrl,id: Path["id"] as! String,userid: UserId,tvshow:(Path["tv_show"] as! Bool),accountstatus:accountInfoStatus)
            }
        }
        else
        {
            let tableview:UITableView = self.superview?.superview as! UITableView
            let viewcontroller = tableview.dataSource as! MMGMainViewController
            let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
            let StorePage = storyBoard.instantiateViewController(withIdentifier: "Store") as! MMGStoreViewController
            StorePage.prodData = Path
            StorePage.userId = UserId
            viewcontroller.navigationController?.pushViewController(StorePage, animated: true)
        }
    }
    
    func carousalSelected(carousalName:String,carousalDetailDict:[[String:Any]],userid:String,deviceid:String,uuid:String)
    {
        self.menuCollectionList.removeAllObjects()
        self.UserId = userid
        self.DeviceId = deviceid
        self.CarousalDict = carousalDetailDict
        self.uuid = uuid
        for dict in carousalDetailDict
        {
            if carousalName == "Menu"
            {
                self.menuCollectionList.add(dict)
            }
            else
            {
                if carousalName == "My List"
                {
                    if dict[kData] != nil
                    {
                        let dicton = dict[kData] as! NSDictionary
                        self.menuCollectionList.add(dicton)
                        self.isMyList = true
                    }
                }
                else if carousalName == "Recently Watched"
                {
                    if dict[kData] != nil
                    {
                        let dicton = dict[kData] as! NSDictionary
                        self.menuCollectionList.add(dicton)
                    }
                    else
                    {
                        self.menuCollectionList.add(dict)
                    }
                }
                else if carousalName == "Store"
                {
                    self.menuCollectionList.add(dict)
                  //  self.menuCollectionList.add(CarousalDict)
                  
                }
                else
                {
                    let metaDict = dict[kMetadata] as! NSDictionary
                    if (metaDict[kCarouselId] as! String) == carousalName
                    {
                        self.menuCollectionList.add(dict)
                    }
                }
            }
        }
        DispatchQueue.main.async {
            self.menuCollectionView.isHidden = false
            self.menuCollectionView.reloadData()
            
        }
    }
    
    // Service Call for getAssetData
    func getAssetData(withUrl:String,id:String,userid:String,/*tvshow:Bool,*/subscription_status:String,list:NSMutableArray,rowid:Int)
    {
        //        if accountstatus == true
        //        {
        var parameters =  [String:[String:AnyObject]]()
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MMGMainViewController
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let osVersion = UIDevice.current.systemVersion
        let className = NSStringFromClass(self.classForCoder)
     
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        MMGApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
                let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! MMGDetailPageViewController
                let jsonresponse = JSON as! NSArray
                for dict in jsonresponse
                {
                    DetailPage.TvshowPath = dict as! NSDictionary
                }
                DetailPage.userid = self.UserId
                DetailPage.deviceId = self.DeviceId
                DetailPage.uuid = self.uuid
                DetailPage.collectionList = list
                DetailPage.rowid = rowid
                DetailPage.isMyList = self.isMyList
                
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MMGMainViewController
                DetailPage.delegate = viewcontroller
                DetailPage.isMain = true
                viewcontroller.navigationController?.pushViewController(DetailPage, animated: true)
            }
            else
            {
                let listError = (error?.localizedDescription)! as String
                UserDefaults.standard.set(listError, forKey: "locerror")
                UserDefaults.standard.synchronize()
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    viewcontroller.navigationController?.popToRootViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                // viewcontroller.navigationController?.pushViewController(alertview, animated: true)
                viewcontroller.present(alertview, animated: true, completion: nil)
                parameters =  ["insertLog":["source":("\(withUrl) - \(className) - \(#function)") as AnyObject,"error":(listError) as AnyObject,"code":parameters as AnyObject,"details":("userId:\(userid) - AppVersion:\(appVersionString) - OSVersion:\(osVersion)") as AnyObject,"device":"AppleTV" as AnyObject]]
                MMGApiManager.sharedManager.postDataWithJson(url: kLogUrl, parameters: parameters)
                {(responseDict,error,isDone)in
                    if error == nil
                    {
                        _ = responseDict
                    }
                }
            }
        }
    }
    
    func getaccountInfo(id:String,userid:String,list:NSMutableArray,rowid:Int)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":DeviceId as AnyObject,"uuid":uuid as AnyObject]]
        MMGApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse == true
                {
                   // self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid,/*tvshow:tvshow,*/subscription_status:(dict.value(forKey: "subscription_status") as! String,nextVideoId:nextData))
                    self.getAssetData(withUrl: kAssestDataUrl, id: id, userid: userid, subscription_status:(dict.value(forKey: "subscription_status") as! String),list:list,rowid:rowid)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
        }
        
    }
    
    // Categories Controller
    func gotoCategories()
    {
        let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
        let CategoriesPage = storyBoard.instantiateViewController(withIdentifier: "Categories") as! MMGCategoriesViewController
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MMGMainViewController
        CategoriesPage.CategoryCarousalName = viewcontroller.CategoryCarousalName
        CategoriesPage.CarousalData = viewcontroller.CarousalData
        CategoriesPage.UserId = viewcontroller.userId
        CategoriesPage.uuid = viewcontroller.uuid
        CategoriesPage.deviceId = viewcontroller.deviceId
        CategoriesPage.MyListthumb = viewcontroller.myList
        CategoriesPage.categoryDelegate = viewcontroller
        viewcontroller.navigationController?.pushViewController(CategoriesPage, animated: true)
    }
    
}
