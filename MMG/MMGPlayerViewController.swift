//
//  ASPlayerViewController.swift
//  Sonic Pool
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import Foundation
import AVKit

protocol playerdelegate:class {
    func getassetdata(withUrl:String,id:String,userid:String)
}

class MMGPlayerViewController: AVPlayerViewController, AVPlayerViewControllerDelegate {
    
    var videoUrl,userID,videoID,mainVideoID,deviceID : String!
    var seektime = Float64()
    var updatetime = Float64()
    var isResume = Bool()
    var detdelegate:playerdelegate?
    var resumeTime = Float64()
    var getnextAsset = Bool()
    var UpdateTimer = Timer()
    var isplayEnd = Bool()
    var nextAsset = NSDictionary()
    var isMyList = Bool()
    
    var mycollectionList = NSMutableArray()
    var rowID = Int()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print(deviceID)
        print(resumeTime)
        print(isMyList)
    }
    
    // playvideo Implementation
    func playVideo(userId:String,videoId:String,mycollectionlist:NSMutableArray,rowid:Int) {
        let playerItem = AVPlayerItem(url: NSURL(string: videoUrl)! as URL)
        userID = userId
        videoID = videoId
        mycollectionList = mycollectionlist
        rowID = rowid
        player = AVPlayer(playerItem: playerItem)
        if isResume
        {
//            var sTime = Float64()
//            if (UserDefaults.standard.object(forKey: "seektime") != nil)
//            {
//                sTime = UserDefaults.standard.object(forKey: "seektime") as! Float64
//            }
            let targetTime = CMTime(seconds: resumeTime, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            player?.seek(to:targetTime , toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
            player?.play()
            
        }
        else
        {
            player?.play()
        }
        
        player?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 1), queue: DispatchQueue.main, using:
            {_ in
                if self.player?.currentItem?.status == .readyToPlay
                {
                    self.seektime = CMTimeGetSeconds(self.player!.currentTime())
                    let duration = CMTimeGetSeconds((self.player?.currentItem?.duration)!)
                    if ((duration - self.seektime) <= 30.0) && (self.getnextAsset == false)
                    {
                            self.nextAssetData()
                    }
                    
                }
        })
        NotificationCenter.default.addObserver(self, selector:#selector(self.playerEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

        
      UpdateTimer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.UpdateseekTime), userInfo: nil, repeats: true)
    }
    
    // seektime
    func UpdateseekTime()
    {
        let parameters = ["updateSeekTime":["userId": userID as AnyObject, "videoId": (videoID) as AnyObject, "seekTime": self.seektime]]
        MMGApiManager.sharedManager.postDataWithJson(url: kUpdateseekUrl, parameters: parameters as [String : [String : AnyObject]]){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["watchedVideo"] != nil
                {
                    self.updatetime = Float64((((JSON)["watchedVideo"] as! NSDictionary)["seekTime"]) as! Float64)
                    print(self.seektime)
//                    UserDefaults.standard.set(self.updatetime, forKey: "seektime")
//                    UserDefaults.standard.synchronize()
                    if self.updatetime > 0
                    {
                        self.detdelegate?.getassetdata(withUrl:kAssestDataUrl,id:self.videoID,userid:self.userID)
                    }
                    if self.isplayEnd
                    {
                        self.detdelegate?.getassetdata(withUrl:kAssestDataUrl,id:self.videoID,userid:self.userID)
                        self.navigationController?.popViewController(animated: true)
                       
                    }
                }
            }
            else
            {
                print("json error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                    self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    // Next Asset Data
    func nextAssetData()
    {
        print("Iam in next Asset Data")
        getnextAsset = true
        
        let parameters = ["getNextPlay": ["videoId": videoID, "userId": self.userID,"myList":self.isMyList,"deviceId":self.deviceID]]
        print(parameters)
        MMGApiManager.sharedManager.postDataWithJson(url:kNextAssetUrl ,parameters: parameters as [String : [String : AnyObject]])
        {
           (responseDict,error,isDone)in
            if error == nil
            {
               self.nextAsset = responseDict as! NSDictionary
            }
            else
            {
                
            }
        }
        
    }
    // player end
    func playerEnd()
    {
        print("Iam in player end")
        seektime = 0.0
        self.isplayEnd = true
        UpdateseekTime()
        print("iam from player end func")
        NotificationCenter.default.removeObserver(self)
        UpdateTimer.invalidate()
       
       // self.navigationController?.popViewController(animated: true)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let playerEnd = storyBoard.instantiateViewController(withIdentifier: "playerEnd") as! MMGPlayerEndViewController
        playerEnd.nextCollectionList = mycollectionList
        playerEnd.nextrowID = rowID
        playerEnd.userID = userID
        print(nextAsset)
        playerEnd.getData(getnextData: self.nextAsset)
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.pushViewController(playerEnd, animated: true)
    }
}
